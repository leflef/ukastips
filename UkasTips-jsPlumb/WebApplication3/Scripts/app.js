﻿/// <reference path="jquery-1.7.1-vsdoc.js" />
jsPlumb.bind("ready", function () {



    jsPlumb.Defaults.Connector = "StateMachine";
    jsPlumb.importDefaults({
        Endpoint: ["Dot", { radius: 2 }],
        PaintStyle: { strokeStyle: "#FFa62c", lineWidth: 2 },
        HoverPaintStyle: { strokeStyle: "#42a62c", lineWidth: 2 },
        ConnectionOverlays: [
    					["Arrow", {
    					    location: 1,
    					    id: "arrow",
    					    length: 14,
    					    foldback: 0.8
    					}],
    	                ["Label", { label: "Test", id: "label" }]
        ]
    });


    jsPlumb.draggable($('.step'));
    var noOfSteps = $('.step').length;
    $('.step').each(function (i, e) {
        if (i < noOfSteps - 1) {
            var stepIdFrom = "step" + i.toString();
            var stepIdTo = "step" + (i + 1).toString();
            var startEndPoint = jsPlumb.addEndpoint(stepIdFrom, {

                paintStyle: { fillStyle: "red" },
                anchor: "Continuous"

            });
            var endEndPoint = jsPlumb.addEndpoint(stepIdTo, {

                paintStyle: { fillStyle: "red" },
                anchor: "Continuous"

            });
            jsPlumb.connect({ source: startEndPoint, target: endEndPoint });
        }
    });


});






















//jsPlumb.Defaults.Connector = "StateMachine";
//jsPlumb.importDefaults({
//    Endpoint: ["Dot", { radius: 2 }],
//    PaintStyle: { strokeStyle: "#FFa62c", lineWidth: 2 },
//    HoverPaintStyle: { strokeStyle: "#42a62c", lineWidth: 2 },
//    ConnectionOverlays: [
//					["Arrow", {
//					    location: 1,
//					    id: "arrow",
//					    length: 14,
//					    foldback: 0.8
//					}],
//	                ["Label", { label: "FOO", id: "label" }]
//    ]
//});


//jsPlumb.draggable($('.step'));